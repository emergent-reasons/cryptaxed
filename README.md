Convert your pile of csv transaction exports into a coherent view and then extract US tax requirements from it.

This is not documented at all and it is not especially easy to use. However, it does three helpful things:

1. Allow you to check your tax requirements without publishing your transactions to a third party tax service.
2. Allow you to double check the conclusions of a third part tax service if you choose to use one.
3. Allow you to see all of your transactions in one place.
