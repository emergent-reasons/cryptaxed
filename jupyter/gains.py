from collections import namedtuple, deque

import pandas as pd

from cryptypes import (standard_sort,
                       TX,
                       INCOME, FORK, MINING, BUY, EXCHANGE_BUY, GIFT_IN,
                       SELL, EXCHANGE_SELL, GIFT_OUT,
                       LONG, SHORT)


FIFO = 'FIFO'
LIFO = 'LIFO'

COL_INSTRUMENT =  'instrument'
COL_SALE_AMOUNT = 'sale_amount'
COL_SALE_VALUE =  'sale_value'
COL_BASIS_VALUE = 'basis_value'
COL_BASIS_UTC =   'basis_UTC'
COL_SALE_UTC =    'sale_UTC'
COL_TERM =        'term'
COL_GAIN =        'gain'

Event = namedtuple('Event', ('id', 'instrument', 'type', 'UTC', 'amount', 'rate', 'source_notes'))
Gain = namedtuple('Gain', (
    COL_INSTRUMENT,

    'basis_id',
    'basis_amount',

    'sale_id',
    COL_SALE_AMOUNT,

    'basis_type',
    COL_BASIS_VALUE,
    COL_SALE_VALUE,

    COL_TERM,
    COL_GAIN,

    'basis_rate',
    'sale_rate',

    COL_BASIS_UTC,
    COL_SALE_UTC,
    'source_notes'
))

MINIMUM_ABS_GAIN_USD = 1

# cost basis types indicate that we should add to the sequence of cost bases that future events will pull from
_cost_basis_tx_types = [INCOME, FORK, MINING, BUY, EXCHANGE_BUY, GIFT_IN]

# drain types indicate that we should drain the amount from the sequence of cost bases
_drain_tx_types = [SELL, EXCHANGE_SELL, GIFT_OUT]

# gain types indicate that a given drain is also a gain event
_gain_types = [SELL, EXCHANGE_SELL]


def dataframe(transactions, method):
    gains = records(transactions, method)
    return pd.DataFrame.from_records(gains, columns=Gain._fields)


def records(transactions, method):
    cost_bases = _CostBases(method)
    for event in _ordered_events(transactions):
        if event.type == TX:
            continue
        elif event.type in _cost_basis_tx_types:
            cost_bases.add(event)
        elif event.type in _drain_tx_types:
            for gain in cost_bases.drain(event):
                if (event.type in _gain_types) and (abs(gain.gain) > MINIMUM_ABS_GAIN_USD):
                    yield gain
                else:
                    pass
        else:
            raise ValueError(f'unhandled event type: {event.type}')


def _ordered_events(transactions):
    tx_cols = tuple(transactions.columns)
    for i, r in enumerate(standard_sort(transactions).itertuples(index=False)):
        utc =        r[tx_cols.index('UTC')]
        txtype =     r[tx_cols.index('tx_type')]
        instrument = r[tx_cols.index('instrument')]
        amount =     abs(r[tx_cols.index('amount')])
        usd_on_day = r[tx_cols.index('USD_per_instrument')]
        source_notes = r[tx_cols.index('source_notes')]

        yield Event(i, instrument, txtype, utc, amount, usd_on_day, source_notes)


class _CostBases:
    def __init__(self, method):
        assert method in (FIFO, LIFO), method
        self._bases_by_instrument = {}
        self._method = method

    def add(self, event):
        assert event.type in _cost_basis_tx_types, event
        instrument_bases = self._bases_by_instrument.setdefault(event.instrument, deque())
        instrument_bases.append(event)

    def drain(self, sale):
        assert sale.type in _drain_tx_types, sale
        bases = self._bases_by_instrument[sale.instrument]

        reappend_for_method, pop_for_method = {
            FIFO: (bases.appendleft, bases.popleft),
            LIFO: (bases.append, bases.pop),
        }[self._method]

        gains = []
        while sale is not None:
            basis = pop_for_method()
            if basis.amount > sale.amount:
                # basis is adjusted
                reappend_for_method(Event(
                    id=basis.id,
                    instrument=basis.instrument,
                    type=basis.type,
                    UTC=basis.UTC,
                    amount=basis.amount - sale.amount,
                    rate=basis.rate,
                    source_notes=basis.source_notes,
                ))
                # sale is consumed
                adjusted_sale = None
                # gain is limited to the sale amount
                gain_amount = sale.amount
            elif basis.amount == sale.amount:
                # basis and sale are consumed
                adjusted_sale = None
                gain_amount = sale.amount
            else:
                # basis is consumed
                # sale is adjusted
                adjusted_sale = Event(
                    id=sale.id,
                    instrument=sale.instrument,
                    type=sale.type,
                    UTC=sale.UTC,
                    amount=sale.amount - basis.amount,
                    rate=sale.rate,
                    source_notes=sale.source_notes,
                )
                # gain is limited to the basis amount
                gain_amount = basis.amount

            sale_value = gain_amount * sale.rate
            basis_value = gain_amount * basis.rate
            gains.append(Gain(
                instrument=basis.instrument,

                basis_id=basis.id,
                basis_amount=basis.amount,

                sale_id=sale.id,
                sale_amount=sale.amount,

                basis_type=basis.type,
                basis_value=basis_value,
                sale_value=sale_value,
                term=_short_or_long(basis, sale),
                gain=sale_value - basis_value,

                basis_rate=basis.rate,
                sale_rate=sale.rate,
                basis_UTC=basis.UTC,
                sale_UTC=sale.UTC,
                source_notes=sale.source_notes,
            ))
            sale = adjusted_sale

        return gains


def _short_or_long(basis, sale):
    return LONG if (sale.UTC - basis.UTC).days > 365 else SHORT
