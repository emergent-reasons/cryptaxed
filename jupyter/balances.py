from collections import namedtuple
import pandas as pd

import arrow

import prices


_APPLY_TO_ROWS = 1


def dataframe(transactions, in_usd=False):
    balances = list(records(transactions, in_usd=in_usd))
    return pd.DataFrame.from_records(balances, columns=balances[-1]._fields)


def records(transactions, in_usd=False):
    transactions['_timestamp'] = transactions.apply(lambda r: r['UTC'].timestamp(), _APPLY_TO_ROWS)
    instruments = list(transactions['instrument'].unique())
    Record = namedtuple('Balance', ['UTC'] + instruments)

    earliest_date = transactions.loc[transactions['_timestamp'].idxmin()]['UTC']
    for week in _weeks(earliest_date):
        # filter up to now
        up_to_now = transactions[transactions['_timestamp'] < week.timestamp()]

        by_instrument_balance = {}
        for instr in instruments:
            # filter by instrument
            by_instrument = up_to_now[up_to_now['instrument'] == instr]
            if len(by_instrument) == 0:
                balance = 0.0
            else:
                balance = float(by_instrument['amount'].sum(skipna=True))
                if in_usd:
                    rate = float(prices.get_usd_at(week, instr))
                    balance = balance * rate
            by_instrument_balance[instr] = balance

        yield Record(**{'UTC': week, **by_instrument_balance})
    transactions.drop(columns='_timestamp')


def _weeks(earliest_date):
    # create index of regular time intervals
    year, month, day = earliest_date.year, earliest_date.month, earliest_date.day
    week = arrow.Arrow(year, month, day, hour=12)
    today = arrow.utcnow()
    while week < today.shift(days=-1):  # price will only be available for yesterday at best
        yield week
        week = week.shift(days=+7)

    # yield the last partial week also
    yield today
