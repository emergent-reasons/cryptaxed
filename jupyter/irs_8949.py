from decimal import Decimal
from math import floor, log10

import arrow

import gains


_APPLY_TO_ROWS = 1

COL_DESCRIPTION = '(a) Description of property'
COL_DATE_ACQUIRED = '(b) Date acquired'
COL_DATE_SOLD = '(c) Date sold or disposed of'
COL_PROCEEDS = '(d) Proceeds'
COL_COST_BASIS = '(e) Cost or other basis'
COL_CODES = '(f) Code(s) from instructions'
COL_ADJUSTMENT = '(g) Amount of adjustment'
COL_GAIN = '(h) Gain or (loss).'
REPORT_COLUMNS = [
    COL_DESCRIPTION,
    COL_DATE_ACQUIRED,
    COL_DATE_SOLD,
    COL_PROCEEDS,
    COL_COST_BASIS,
    COL_CODES,
    COL_ADJUSTMENT,
    COL_GAIN,
]


def dataframe(transactions, year):
    df = gains.dataframe(transactions, gains.FIFO)
    if len(df) == 0:
        return [], 0, [], 0

    # get everything into 8949 formatted columns
    df[COL_DESCRIPTION] =   df.apply(lambda r: '{} {}'.format(_round_to_n_sigfigs(r[gains.COL_SALE_AMOUNT], 4),
                                                              r[gains.COL_INSTRUMENT]), _APPLY_TO_ROWS)
    df[COL_DATE_ACQUIRED] = df.apply(lambda r: '{}, {}, {}'.format(r[gains.COL_BASIS_UTC].month,
                                                                   r[gains.COL_BASIS_UTC].day,
                                                                   r[gains.COL_BASIS_UTC].year), _APPLY_TO_ROWS)
    df[COL_DATE_SOLD] =     df.apply(lambda r: '{}, {}, {}'.format(r[gains.COL_SALE_UTC].month,
                                                                   r[gains.COL_SALE_UTC].day,
                                                                   r[gains.COL_SALE_UTC].year), _APPLY_TO_ROWS)
    df[COL_PROCEEDS] =      df.apply(lambda r: round(r[gains.COL_SALE_VALUE], 0), _APPLY_TO_ROWS)
    df[COL_COST_BASIS] =    df.apply(lambda r: round(r[gains.COL_BASIS_VALUE], 0), _APPLY_TO_ROWS)
    df[COL_CODES] =         ""
    df[COL_ADJUSTMENT] =    ""
    df[COL_GAIN] =          df.apply(_format_gain, _APPLY_TO_ROWS)

    # filter into current year
    year_start, year_end = arrow.Arrow(year, 1, 1), arrow.Arrow(year + 1, 1, 1)
    df['_timestamp'] = df.apply(lambda r: r[gains.COL_SALE_UTC].timestamp(), _APPLY_TO_ROWS)
    df = df[(df['_timestamp'] >= year_start.timestamp()) & (df['_timestamp'] < year_end.timestamp())]
    df = df.drop(columns='_timestamp')

    # filter into short and long
    short = df[df[gains.COL_TERM] == gains.SHORT]
    long = df[df[gains.COL_TERM] == gains.LONG]
    short_total = sum(Decimal(x) for x in short[gains.COL_GAIN])
    long_total = sum(Decimal(x) for x in long[gains.COL_GAIN])

    return short[REPORT_COLUMNS], short_total, long[REPORT_COLUMNS], long_total


def _format_gain(r):
    base = round(r[gains.COL_GAIN], 0)
    if base >= 0:
        return f'{base}'
    else:
        return f'({abs(base)})'


# based on https://stackoverflow.com/a/3411435/377366
def _round_to_n_sigfigs(x, n):
    return round(x, -int(floor(log10(abs(x)))) + (n - 1))
