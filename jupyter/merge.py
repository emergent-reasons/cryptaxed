from decimal import Decimal
from hashlib import sha256

import arrow
import pandas as pd


_APPLY_TO_ROWS = 1
_UPDATE_COLUMNS = ['source_notes', 'tx_type', 'pair_idx', 'amount_USD']
_HASH_COLUMNS = ['wallet', 'UTC', 'instrument', 'amount']
_HASH_COL = '_hash'


def from_manual(base, manual_csv):
    # setup hashed base records
    base = base.reset_index(drop=True)
    base[_HASH_COL] = base.apply(_internal_record_hash, axis=_APPLY_TO_ROWS)
    base = base.set_index(_HASH_COL, drop=True)  # allow duplicate hashes. only a problem if update depends on a dupe

    # get hashed manual updates
    updates = _load_manual_updates(manual_csv)
    update_cols = tuple(updates.columns)

    # look for each update in the base
    for update in updates.itertuples(index=True):
        # look for matching hash
        update_hash = update[0]
        matches_in_base = base[base.index == update_hash]
        if len(matches_in_base) == 1:
            for update_col in _UPDATE_COLUMNS:
                update_value = update[1 + update_cols.index(update_col)]
                if update_value != "":
                    base.loc[base.index == update_hash, update_col] = update_value
        else:
            raise ValueError(f'unexpectedly found {len(matches_in_base)} matches for manual update: {update}')
    base = base.reset_index(drop=True)
    return base


def _load_manual_updates(csv):
    # Date,Type,Reference,Amount,Fee,Total Amount (BCH),Total Amount (USD price at time)
    df = pd.read_csv(
        csv,
        index_col=False,
        converters={
            'source_notes': str,
            'wallet':       str,
            'UTC':          arrow.get,
            'tx_type':      str,
            'instrument':   str,
            'amount':       lambda x: Decimal(x).normalize(),
            'amount_USD':   lambda x: Decimal(x).normalize(),
            'pair_idx':     str,
        }
    )
    assert set(df.columns) == set(_UPDATE_COLUMNS + _HASH_COLUMNS), f'got {set(df.columns)} but expected {set(_UPDATE_COLUMNS + _HASH_COLUMNS)}'
    df[_HASH_COL] = df.apply(_internal_record_hash, axis=_APPLY_TO_ROWS)
    df = df.set_index(_HASH_COL, verify_integrity=True, drop=True)
    return df


def _internal_record_hash(r):
    hash_items = (
        r['wallet'],
        r['UTC'].year,
        r['UTC'].month,
        r['UTC'].day,
        r['UTC'].hour,
        r['UTC'].minute,
        r['UTC'].second,
        r['UTC'].microsecond,
        r['instrument'],
        r['amount'].normalize(),
    )
    hash_source = ';'.join(str(x) for x in hash_items)
    new = sha256(hash_source.encode()).hexdigest()
    return new
