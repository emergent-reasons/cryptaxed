from decimal import Decimal as D

import arrow
import pandas as pd

from cryptypes import (UNKNOWN_NUM, UNKNOWN_STR, decimal_or_nan,
                       BCH, BSV, ETH, XMR,
                       TX,
                       INCOME, MINING, FORK, BUY, EXCHANGE_BUY, GIFT_IN,
                       SELL, EXCHANGE_SELL, GIFT_OUT)
from prices import get_usd_at


_APPLY_TO_ROWS = 1
ACCOUNT_REQUIRED_COLUMNS = [
    'wallet',
    'UTC',
    'tx_type',
    'instrument',
    'amount',
    'fee',

    'amount_USD',
    # 'fee_USD',
    'USD_per_instrument',

    'source_notes',
    'tx_hash',
    'pair_idx',
]


def bitcoincom_account_tx(csv, instrument):
    # Date,Type,Reference,Amount,Fee,Total Amount (BCH),Total Amount (USD price at time)
    df = pd.read_csv(
        csv,
        index_col=False,
        converters={
            'Date':                             str,
            'Type':                             str,
            'Reference':                        str,
            'Amount':                           decimal_or_nan,
            'Fee':                              decimal_or_nan,
            f'Total Amount ({instrument})':     decimal_or_nan,
            'Total Amount (USD price at time)': decimal_or_nan,
        }
    )

    # before processing into final columns, combine Type=={PPS, Contract} so the fee and payout are in one transfer
    df['_UTC-Day'] = df.apply(lambda r: arrow.get(r['Date'], 'DD MMM YYYY'), axis=_APPLY_TO_ROWS)
    # clean split of mining / non-mining
    mining_types = 'PPS Reward', 'Contract Fee'
    allowed_types = ('Manual Withdrawal',) + mining_types
    if any(x not in allowed_types for x in df['Type']):
        raise ValueError(f'unexpected "Type" found in input data; expected one of {allowed_types}')
    mining = df[df['Type'].isin(mining_types)]
    non_mining = df[~df['Type'].isin(mining_types)]
    if len(mining) + len(non_mining) != len(df):
        raise RuntimeError('unexpectedly failed to split full transactions exactly into mining and non-mining')

    # combine the PPS and Contract rows
    mining = mining.groupby('_UTC-Day', as_index=False).agg({
        'Date':                             lambda s: s.iloc[0],  # i.e. take the first if multiple
        'Type':                             lambda s: ', '.join(s),
        'Reference':                        lambda s: ', '.join(s),
        'Amount':                           lambda s: _decimal_or_unknown(sum(D(x) for x in s).normalize()),
        'Fee':                              lambda s: _decimal_or_unknown(sum(D(x) for x in s).normalize()),
        f'Total Amount ({instrument})':     lambda s: _decimal_or_unknown(sum(D(x) for x in s).normalize()),
        'Total Amount (USD price at time)': lambda s: _decimal_or_unknown(sum(D(x) for x in s).normalize()),
    })

    # remove effectively zero mining amounts to keep records simple (they don't round to zero)
    mining = mining[mining['Total Amount (USD price at time)'] >= 0.005]

    # recombine everything into one dataframe
    df = pd.concat([non_mining, mining], sort=True)  # sort re-aligns the two sets of columns

    columns = [
        ('source_notes', lambda r: _collect_notes_columns(r, 'Reference', f'Total Amount ({instrument})',
                                                          'Total Amount (USD price at time)')),
        ('wallet',       lambda r: 'bitcoin.com-mining'),
        ('UTC',          lambda r: arrow.get(r['Date'], 'DD MMM YYYY HH:mm:ss')),
        ('tx_type',      lambda r: MINING if ('pps' in r['source_notes'].lower())
                                   else FORK if ('initial balance' in r['source_notes'].lower())
                                   else TX if ('withdrawal' in r['source_notes'].lower())
                                   else UNKNOWN_STR),
        ('instrument',   lambda r: instrument),
        ('amount',       lambda r: r['Amount']),
        ('fee',          lambda r: r['Fee']),
        ('tx_hash',      lambda r: r['Reference'] if r['Fee'] != 0 else UNKNOWN_STR),
        ('pair_idx',     lambda r: ''),
    ]
    columns.extend(_usd_columns())
    return _build_full_account(df, columns)


def bitflyer_account_tx(csv, instrument):
    # Trade Date, Product, Trade Type,
    # Price, BTC, Fee(BTC), Balance(BTC),
    # JPY, Balance(JPY),
    # ETH, Fee(ETH), Balance(ETH),
    # LTC, Fee(LTC), Balance(LTC),
    # ETC, Fee(ETC), Balance(ETC),
    # BCH, Fee(BCH), Balance(BCH),
    # MONA, Fee(MONA), Balance(MONA),
    # Order ID
    df = pd.read_csv(
        csv,
        index_col=False,
        converters={
            'Trade Date':             str,
            'Trade Type':             str,
            instrument:               decimal_or_nan,
            f'Fee({instrument})':     decimal_or_nan,
            f'Balance({instrument})': decimal_or_nan,
        }
    )

    # create required columns
    columns = [
        ('wallet',       lambda r: 'bitflyer'),
        # bitflyer reports data in Japan time (+9)
        ('UTC',          lambda r: arrow.get(r['Trade Date'], 'MMM D, YYYY HH:mm:ss').shift(hours=-9)),
        ('tx_type',      lambda r: UNKNOWN_STR),
        ('instrument',   lambda r: instrument),
        ('amount',       lambda r: r[instrument]),
        ('fee',          lambda r: r[f'Fee({instrument})']),
        ('source_notes', lambda r: _collect_notes_columns(r, 'Order ID', f'Balance({instrument})')),
        ('tx_hash',      lambda r: UNKNOWN_STR),
        ('pair_idx',     lambda r: ''),
    ]
    columns.extend(_usd_columns())
    return _build_full_account(df, columns)


def coinbase_account_tx(csv, instrument):
    # Timestamp,Balance,Amount,Currency,To,Notes,Instantly Exchanged,Transfer Total,Transfer Total Currency,
    # Transfer Fee,Transfer Fee Currency,Transfer Payment Method,Transfer ID,Order Price,Order Currency,Order BTC,
    # Order Tracking Code,Order Custom Parameter,Order Paid Out,Recurring Payment ID,
    # Coinbase ID (visit https://www.coinbase.com/transactions/[ID] in your browser),
    # Bitcoin Hash (visit https://www.coinbase.com/tx/[HASH] in your browser for more info)
    df = pd.read_csv(
        csv,
        index_col=False,
        converters={
            'Timestamp':               str,
            'Balance':                 decimal_or_nan,
            'Amount':                  decimal_or_nan,
            'Currency':                str,
            'To':                      str,
            'Notes':                   str,

            # Transfer * are only for fiat buy/sell/fee values
            'Transfer Total':          decimal_or_nan,
            'Transfer Total Currency': str,
            'Transfer Fee':            decimal_or_nan,
            'Transfer Fee Currency':   decimal_or_nan,

            # Order * are for only for purchases through coinbase or their partners
            'Order Price':             decimal_or_nan,
            'Order Currency':          decimal_or_nan,
            'Order BTC':               decimal_or_nan,
        }
    )

    columns = [
        ('wallet',       lambda r: 'coinbase'),
        ('UTC',          lambda r: arrow.get(r['Timestamp'], 'YYYY-MM-DD HH:mm:ss Z').to('UTC')),
        ('instrument',   lambda r: instrument),
        ('amount',       lambda r: r['Amount']),
        ('fee',          lambda r: D(0)),
        ('source_notes', lambda r: _collect_notes_columns(r, 'Notes', 'Transfer Total', 'Transfer Fee', 'Order Price')),
        ('tx_type',      lambda r: BUY if ('bought' in r['source_notes'].lower())
                                   else SELL if ('order' in r['source_notes'].lower()) or
                                                ('sold' in r['source_notes'].lower()) or
                                                ('notes' in r['source_notes'].lower() and r['Amount'] < 0)
                                   else TX if ('withdrawal' in r['source_notes'].lower())
                                   else INCOME if ('notes' in r['source_notes'].lower() and r['Amount'] > 0)
                                   else UNKNOWN_STR),
        ('tx_hash',      lambda r: UNKNOWN_STR),
        ('pair_idx',     lambda r: ''),
    ]
    columns.extend(_usd_columns())
    return _build_full_account(df, columns)


def coinex_account(csv, utc_offset):
    # Time, Operation, Coin, Asset change, Balance
    # 2018-01-01 13:13:13, withdraw, BCH, -1.111, 2
    df = pd.read_csv(
        csv,
        index_col=False,
        converters={
            'Time':         str,
            'Operation':    str,
            'Coin':         lambda c: BSV if c == 'BCHSV' else str(c),
            'Asset change': decimal_or_nan,
            'Balance':      decimal_or_nan,
        }
    )
    keep_operations = ('deposit', 'withdraw', 'trade', 'gift')
    ignore_coins = ('USHA', 'USHB', 'EXTRA', 'LV', 'HYDRO')
    df = df.drop(df[(~df['Operation'].isin(keep_operations) | df['Coin'].isin(ignore_coins))].index)

    columns = [
        ('wallet',       lambda r: 'coinex'),
        ('UTC',          lambda r: arrow.get(r['Time'], 'YYYY-MM-DD HH:mm:ss').shift(hours=-utc_offset)),
        ('tx_type',      lambda r: TX if r['Operation'] in ('deposit', 'withdraw')
                                   else EXCHANGE_BUY if (r['Operation'] == 'trade') and (r['Asset change'] > 0)
                                   else EXCHANGE_SELL if (r['Operation'] == 'trade') and (r['Asset change'] < 0)
                                   else INCOME if r['Operation'] == 'gift'
                                   else UNKNOWN_STR),
        ('amount',       lambda r: r['Asset change']),
        ('instrument',   lambda r: r['Coin']),
        ('fee',          lambda r: D(0)),
        ('source_notes', lambda r: ''),
        ('tx_hash',      lambda r: UNKNOWN_STR),
        ('pair_idx',     lambda r: ''),
    ]
    columns.extend(_usd_columns())
    return _build_full_account(df, columns)


def electrum_account_tx(csv, instrument, wallet_name, utc_offset, should_ignore_unlabeled=False):
    datetime_format = 'YYYY-MM-DD HH:mm:ss'
    return _electrum_electron_account_tx(csv, instrument, wallet_name, utc_offset, should_ignore_unlabeled, datetime_format)


def electron_account_tx(csv, instrument, wallet_name, utc_offset, should_ignore_unlabeled=False):
    datetime_format = 'YYYY-MM-DD HH:mm'
    return _electrum_electron_account_tx(csv, instrument, wallet_name, utc_offset, should_ignore_unlabeled, datetime_format)


def _electrum_electron_account_tx(csv, instrument, wallet_name, utc_offset, should_ignore_unlabeled, datetime_format):
    # transaction_hash	label	confirmations	value	timestamp
    df = pd.read_csv(
        csv,
        index_col=False,
        converters={
            'transaction_hash': str,
            'label':            str,
            'confirmations':    decimal_or_nan,
            'value':            lambda x: decimal_or_nan(str(x).replace(' BTC', '')),
            'timestamp':        str
        }
    )

    if should_ignore_unlabeled:
        df = df[df['label'] != '']

    def timestamp_to_utc(row):
        try:
            return arrow.get(row['timestamp'], datetime_format).shift(hours=-utc_offset)
        except arrow.ParserError as e:
            if 'unconfirmed' in str(e):
                return arrow.utcnow()
            raise e

    # create required columns
    columns = [
        ('wallet',       lambda r: wallet_name),
        # correction in case electron uses a local offset when reporting
        ('UTC',          timestamp_to_utc),
        ('instrument',   lambda r: instrument),
        ('amount',       lambda r: r['value']),
        ('fee',          lambda r: UNKNOWN_NUM),
        ('source_notes', lambda r: _collect_notes_columns(r, 'label')),
        ('tx_hash',      lambda r: r['transaction_hash']),
        ('tx_type',      _detect_electrum_tx_type),
        ('pair_idx',     lambda r: ''),
    ]
    columns.extend(_usd_columns())
    return _build_full_account(df, columns)


def etherscan_account_tx(csv, wallet_name):
    # "Txhash","Blockno","UnixTimestamp","DateTime","From","To","ContractAddress","Value_IN(ETH)","Value_OUT(ETH)",
    # "CurrentValue @ $457.24/Eth","TxnFee(ETH)","TxnFee(USD)","Historical $Price/Eth","Status","ErrCode"
    df = pd.read_csv(
        csv,
        index_col=False,
        converters={
            'Txhash':          str,
            'Blockno':         str,
            'UnixTimestamp':   str,
            'DateTime':        str,
            'From':            str,
            'To':              str,
            'ContractAddress': str,
            'Value_IN(ETH)':   decimal_or_nan,
            'Value_OUT(ETH)':  decimal_or_nan,
            'TxnFee(ETH)':     decimal_or_nan,
        }
    )
    # make sure each row has exactly one non-zero value
    if len(df) != (df['Value_IN(ETH)'] != 0).sum() + (df['Value_OUT(ETH)'] != 0).sum():
        raise RuntimeError('unexpected overlap between value in and value out')
    # make sure all values are zero or positive
    if any(df['Value_IN(ETH)'] < 0) or any(df['Value_OUT(ETH)'] < 0):
        raise RuntimeError('unexpectedly found negative in/out values')

    columns = [
        ('wallet',       lambda r: wallet_name),
        ('UTC',          lambda r: arrow.get(r['UnixTimestamp'], 'X')),
        ('tx_type',      lambda r: UNKNOWN_STR),
        ('instrument',   lambda r: ETH),
        ('amount',       lambda r: r['Value_IN(ETH)'] if (r['Value_IN(ETH)'] > 0) else -r['Value_OUT(ETH)']),
        ('fee',          lambda r: r['TxnFee(ETH)']),
        ('source_notes', lambda r: _collect_notes_columns(r, 'Blockno')),
        ('tx_hash',      lambda r: r['Txhash']),
        ('pair_idx',     lambda r: ''),
    ]
    columns.extend(_usd_columns())
    return _build_full_account(df, columns)


def gdax_account(csv, instrument):
    # type, time, amount, balance, amount/balance unit, transfer id, trade id, order id
    df = pd.read_csv(
        csv,
        index_col=False,
        converters={
            'type':                str,
            'time':                str,
            'amount':              decimal_or_nan,
            'balance':             decimal_or_nan,
            'amount/balance unit': str,
            'transfer id':         str,
            'trade id':            str,
            'order id':            str,
        }
    )

    columns = [
        ('wallet',       lambda r: 'gdax'),
        ('UTC',          lambda r: arrow.get(r['time'])),  # their times are in UTC
        ('tx_type',      lambda r: BUY if (r['type'] == 'match') and (r['amount'] > 0)
                                   else SELL if (r['type'] == 'match') and (r['amount'] < 0)
                                   else TX if (r['type'] in ('deposit', 'withdrawal'))
                                   else UNKNOWN_STR),
        ('instrument',   lambda r: instrument),
        ('fee',          lambda r: D(0)),
        ('source_notes', lambda r: _collect_notes_columns(r, 'type', 'balance', 'transfer id', 'trade id', 'order id')),
        ('tx_hash',      lambda r: UNKNOWN_STR),
        ('pair_idx',     lambda r: ''),
    ]
    columns.extend(_usd_columns())
    return _build_full_account(df, columns)


def hit_account_tx(csv, instrument):
    # Date (-06),Operation id,Type,Amount,Transaction Hash,Main account balance,Instrument
    # (have to add Instrument manually)

    # offset is hidden in the header like this: Date (-06)
    with open(csv, 'r') as f:
        date_header = f.readline().split(',')[0]
    import re
    utc_offset = int(re.findall(r'([+-][0-9]{2})', date_header)[0])
    if not (-12 <= utc_offset <= 12):
        raise ValueError(f'unexpected utc offset value: {utc_offset}')

    df = pd.read_csv(
        csv,
        index_col=False,
        converters={
            date_header:            str,
            'Operation id':         str,
            'Type':                 str,
            'Amount':               decimal_or_nan,
            'Transaction Hash':     str,
            'Main account balance': decimal_or_nan,
            'Instrument':           str,
        }
    )
    # remove BTG
    df = df[df['Instrument'] != 'BTG']

    # remove trading account transfers (internal zero-cost transfers)
    df = df[df['Type'].isin(('Withdraw', 'Deposit'))]
    if len(df[df['Type'] == 'Withdraw']) + len(df[df['Type'] == 'Deposit']) <= 0:
        raise RuntimeError('unexpectedly filtered all transactions when trying to filter internal transfers')

    columns = [
        ('wallet',       lambda r: 'hitbtc'),
        ('UTC',          lambda r: arrow.get(r[date_header], 'YYYY-MM-DD H:mm:ss').shift(hours=-utc_offset)),
        ('tx_type',      lambda r: UNKNOWN_STR),  # All Tx except fork which is indistinguishable from deposit
        ('instrument',   lambda r: _parse_hit_tickers(r['Instrument'])),
        ('amount',       lambda r: r['Amount']),
        ('fee',          lambda r: UNKNOWN_NUM),
        ('source_notes', lambda r: _collect_notes_columns(r, 'Operation id')),
        ('tx_hash',      lambda r: r['Transaction Hash']),
        ('pair_idx',     lambda r: ''),
    ]
    columns.extend(_usd_columns())
    return _build_full_account(df, columns)


def _parse_hit_tickers(t):
    if t == 'BCHSV':
        return BSV
    elif t == 'BCHABC':
        return BCH
    return t


def hit_account_ex(csv):
    # Date (-06), Instrument, Trade ID, Order ID, Side, Quantity, Price, Volume, Fee, Rebate, Total

    # hit exchanges have left/right details following the fixed order of the pair (e.g. BCH/BTC)
    # e.g. if it is a 'buy' transaction, that means buy BCH and sell BTC for that pair
    # therefore split each exchange into separate left/right transactions

    # the left side amount is in the "Quantity" field
    df_left = _base_hit_exchange_df(csv)
    columns = [
        ('wallet',       lambda r: 'hitbtc'),
        ('tx_type',      lambda r: EXCHANGE_BUY if r['Side'] == 'buy'
                                   else EXCHANGE_SELL if r['Side'] == 'sell'
                                   else UNKNOWN_STR),
        ('instrument',   lambda r: _parse_hit_tickers(r['Instrument'].split('/')[0].strip())),
        ('amount',       lambda r: r['Quantity'] if r['tx_type'] == EXCHANGE_BUY else -r['Quantity']),
        ('fee',          lambda r: UNKNOWN_NUM),
        ('source_notes', lambda r: _collect_notes_columns(r, 'Trade ID', 'Order ID')),
        ('tx_hash',      lambda r: UNKNOWN_STR),
        ('pair_idx',     lambda r: ''),
    ]
    columns.extend(_usd_columns())
    df_left = _build_full_account(df_left, columns)
    _bad_row_count = len(df_left[df_left['tx_type'] == UNKNOWN_STR])
    if _bad_row_count != 0:
        raise ValueError(f'unexpectedly unable to identify tx type for {_bad_row_count} exchange left-sides')

    # the right side amount is in the "Volume" field
    df_right = _base_hit_exchange_df(csv)
    columns = [
        ('wallet',       lambda r: 'hitbtc'),
        ('tx_type',      lambda r: EXCHANGE_SELL if r['Side'] == 'buy'  # reverse buy/sell for right side of exchange
                                   else EXCHANGE_BUY if r['Side'] == 'sell'
                                   else UNKNOWN_STR),
        ('instrument',   lambda r: _parse_hit_tickers(r['Instrument'].split('/')[1].strip())),
        ('amount',       lambda r: r['Volume'] if r['tx_type'] == EXCHANGE_BUY else -r['Volume']),
        ('fee',          lambda r: UNKNOWN_NUM),
        ('source_notes', lambda r: _collect_notes_columns(r, 'Trade ID', 'Order ID')),
        ('tx_hash',      lambda r: UNKNOWN_STR),
        ('pair_idx',     lambda r: ''),
    ]
    columns.extend(_usd_columns())
    df_right = _build_full_account(df_right, columns)
    _bad_row_count = len(df_right[df_right['tx_type'] == UNKNOWN_STR])
    if _bad_row_count != 0:
        raise ValueError(f'unexpectedly unable to identify tx type for {_bad_row_count} exchange right-sides')

    df = pd.concat([df_left, df_right], ignore_index=True)
    assert len(df) == len(df_left) + len(df_right)
    return df


def _base_hit_exchange_df(csv):
    # Date (-06), Instrument, Trade ID, Order ID, Side, Quantity, Price, Volume, Fee, Rebate, Total

    # offset is hidden in the header like this: Date (-06)
    with open(csv, 'r') as f:
        date_header = f.readline().split(',')[0].replace('"', '')
    import re
    utc_offset = int(re.findall(r'([+-][0-9]{2})', date_header)[0])
    if not (-12 <= utc_offset <= 12):
        raise ValueError(f'unexpected utc offset value: {utc_offset}')

    df = pd.read_csv(
        csv,
        index_col=False,
        converters={
            date_header:  str,
            'Instrument': str,  # "{left instrument}/{right instrument}"
            'Trade ID':   str,
            'Order ID':   str,
            'Side':       str,
            'Quantity':   decimal_or_nan,     # amount for "Side" (buy or sell), same as {left instrument}
            'Price':      decimal_or_nan,
            'Volume':     decimal_or_nan,     # amount for right side of pair
            'Fee':        decimal_or_nan,
            'Rebate':     decimal_or_nan,
            'Total':      decimal_or_nan,
        }
    )
    df['UTC'] = df.apply(lambda r: arrow.get(r[date_header], 'YYYY-MM-DD H:mm:ss')
                                        .shift(hours=-utc_offset), axis=_APPLY_TO_ROWS)
    return df


def mymonero_account_tx(csv, wallet_name, utc_offset):
    # Value (XMR),Date Received,Mixin,Transaction ID
    df = pd.read_csv(
        csv,
        index_col=False,
        converters={
            'Value (XMR)':    decimal_or_nan,
            'Date Received':  str,
            'Mixin':          str,
            'Transaction ID': str,
        }
    )

    columns = [
        ('wallet',       lambda r: wallet_name),
        # MyMonero silently adjusts to local time
        ('UTC',          lambda r: arrow.get(r['Date Received'], 'DD MMMM YYYY HH:mm:ss')
                                        .shift(hours=-utc_offset)),
        ('tx_type',      lambda r: UNKNOWN_STR),
        ('instrument',   lambda r: XMR),
        ('amount',       lambda r: r['Value (XMR)']),
        ('fee',          lambda r: UNKNOWN_NUM),
        ('source_notes', lambda r: _collect_notes_columns(r, 'Mixin')),
        ('tx_hash',      lambda r: r['Transaction ID']),
        ('pair_idx',     lambda r: ''),
    ]
    columns.extend(_usd_columns())
    return _build_full_account(df, columns)


def trezor_account_tx(csv, instrument, data_utc_offset, wallet_name):
    # Date,Time,TX id,Address,TX type,Value,TX total,Fee,Balance
    df = pd.read_csv(
        csv,
        index_col=False,
        converters={
            'Date':     str,
            'Time':     str,
            'TX id':    str,
            'Address':  str,
            'TX type':  str,
            'Value':    decimal_or_nan,
            'TX total': decimal_or_nan,
            'Fee':      decimal_or_nan,
            'Balance':  decimal_or_nan,
        }
    )

    # create required columns
    columns = [
        ('wallet',       lambda r: wallet_name),
        # trezor silently adjusts to local time. not documented in trezor data
        ('UTC',          lambda r: arrow.get(r['Date'] + ' ' + r['Time'], 'YYYY-MM-DD HH:mm:ss')
                                        .shift(hours=-data_utc_offset)),
        ('tx_type',      lambda r: UNKNOWN_STR),
        ('instrument',   lambda r: instrument),
        ('amount',       lambda r: r['Value'] if r['TX type'] == 'IN' else -r['Value']),
        ('fee',          lambda r: r['Fee']),
        ('source_notes', lambda r: _collect_notes_columns(r, 'Balance')),
        ('tx_hash',      lambda r: r['TX id']),
        ('pair_idx',     lambda r: ''),
    ]
    columns.extend(_usd_columns())
    return _build_full_account(df, columns)


def manual_account_tx(csv):
    df = pd.read_csv(
        csv,
        index_col=False,
        converters={
            'wallet':       str,
            'UTC':          lambda x: arrow.get(x).to('UTC'),
            'tx_type':      str,
            'instrument':   str,
            'amount':       D,
            'fee':          D,
            'source_notes': str,
            'tx_hash':      str,
            'pair_idx':     str,
        }
    )
    columns = _usd_columns()
    return _build_full_account(df, columns)


def _usd_columns():
    return (
        ('USD_per_instrument', lambda r: get_usd_at(r['UTC'], r['instrument'])),
        ('amount_USD',         lambda r: r['USD_per_instrument'] * r['amount']),
        ('fee_USD',            lambda r: r['USD_per_instrument'] * r['fee']),
    )


def _collect_notes_columns(r, *cols):
    return ', '.join('{}{}'.format(
        '' if col == 'label' else f'{col}:',
        str(r[col]).replace('\n', '').strip())
        for col in cols
        if str(r[col]).replace('\n', '').strip() not in ("", UNKNOWN_STR, str(UNKNOWN_NUM)))


def _build_full_account(df, column_creators):
    # apply all account-specific details
    for col, creator in column_creators:
        df[col] = df.apply(creator, axis=_APPLY_TO_ROWS)

    _assert_all_columns_available(df)
    return df[ACCOUNT_REQUIRED_COLUMNS]


def _assert_all_columns_available(df):
    remain_cols = [c for c in ACCOUNT_REQUIRED_COLUMNS if c not in df.columns]
    if len(remain_cols) != 0:
        raise ValueError(f'some required columns are missing: {remain_cols}')


def _decimal_or_unknown(value):
    if not isinstance(value, D):
        raise TypeError(f'unexpected non-decimal usd value: {type(value)}')
    return decimal_or_nan(value)


def _detect_electrum_tx_type(row):
    content = row['label'].split()
    head = content[0].lower() if content else ''

    amount = row['amount']
    if any(head.startswith(x.lower()) for x in ('shuffle', 'cashfusion', TX)):
        return TX
    elif any(head.startswith(x.lower()) for x in ('buyfor', 'buy', BUY, EXCHANGE_BUY)):
        if amount < 0:
            raise ValueError(f'Buy transaction has negative value: {row}')
        return BUY
    elif any(head.startswith(x.lower()) for x in ('sellfor', 'sell', SELL, EXCHANGE_SELL)):
        if amount > 0:
            raise ValueError(f'Sell transaction has positive value: {row}')
        return SELL
    elif any(head.startswith(x.lower()) for x in ('tip', 'donat', 'gift')):
        if amount > 0:
            return GIFT_IN
        else:
            return GIFT_OUT
    elif head.startswith(INCOME.lower()):
        if amount < 0:
            raise ValueError(f'Income transaction has negative value: {row}')
        return INCOME
    elif head.startswith(MINING.lower()):
        if amount < 0:
            raise ValueError(f'Mining transaction has negative value: {row}')
        return INCOME
    elif head.startswith(FORK.lower()):
        if amount < 0:
            raise ValueError(f'Fork transaction has negative value: {row}')
        return FORK
    else:
        return UNKNOWN_STR
