from decimal import Decimal

import arrow
import pandas as pd

from cryptypes import BTC, BCH, BSV, CET, ETH, USDT, XMR, decimal_or_nan


# get the base price data
# snapped_at	price	market_cap	total_volume
_converters = {
    'price': decimal_or_nan,
    'snapped_at': lambda d: arrow.get(d, 'YYYY-MM-DD'),  # ignore hours etc.
}

_price_records = {
    BCH: pd.read_csv('price_records/bch.csv', index_col=False, converters=_converters),
    BSV: pd.read_csv('price_records/bsv.csv', index_col=False, converters=_converters),
    BTC: pd.read_csv('price_records/btc.csv', index_col=False, converters=_converters),
    CET: pd.read_csv('price_records/cet.csv', index_col=False, converters=_converters),
    ETH: pd.read_csv('price_records/eth.csv', index_col=False, converters=_converters),
    USDT: pd.read_csv('price_records/usdt.csv', index_col=False, converters=_converters),
    XMR: pd.read_csv('price_records/xmr.csv', index_col=False, converters=_converters),
}

# add early data to BCH for futures before market stabilized
_early_records = {
    'BCH': {
        (2017, 7, 29): Decimal(400),
        (2017, 7, 30): Decimal(400),
        (2017, 7, 31): Decimal(400),
        (2017, 8, 1): Decimal(400),
    },
    'BSV': {
        (2018, 11, 15): Decimal(70.00),
    },
}


def get_usd_at(utc, instrument):
    utc_date_only = arrow.Arrow(utc.year, utc.month, utc.day)
    utc_key = (utc.year, utc.month, utc.day)

    # check for early values where there was little/no consensus on price - have to use actual exchange value
    early = _early_records.get(instrument, {}).get(utc_key)
    if early:
        return early

    # normally lookup the market value
    r = _price_records[instrument]
    matches = r[r['snapped_at'] == utc_date_only]
    if len(matches) == 0:
        # look for early bch values
        try:
            return _early_records[instrument][utc_key]
        except KeyError:
            pass
    if len(matches) != 1:
        raise ValueError(f'unexpected {len(matches)} matches for conversion ({instrument} @ {utc_date_only}))')
    return Decimal(matches.iloc[0]['price'])


# test bad date lookup
try:
    _test_result = get_usd_at(arrow.Arrow(1, 1, 1), BCH)
    raise RuntimeError(f'unexpectedly did not fail with bad price lookup for BCH @ 0001-01-01: {_test_result}')
except ValueError:
    pass

# test valid lookup
for instrument, price_spec in (
        (BCH, Decimal('181.37912999671067')),
        (BSV, Decimal('60.092326428875396')),
        (BTC, Decimal('3895.8368129589876')),
        (CET, Decimal('0.005976391463062957')),
        (ETH, Decimal('114.28793125942474')),
        (USDT, Decimal('0.9846772135647424')),
        (XMR, Decimal('59.83335922541085'))):
    price = get_usd_at(arrow.Arrow(2018, 11, 25), instrument)
    assert price == price_spec, f'wrong {instrument} price: {price} instead of {price_spec}'

