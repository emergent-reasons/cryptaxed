from decimal import Decimal, InvalidOperation


BCH = 'BCH'
BSV = 'BSV'
BTC = 'BTC'
CET = 'CET'
ETH = 'ETH'
USDT = 'USDT'
XMR = 'XMR'
UNKNOWN_STR = '?'

_TWO_DIGITS = Decimal('0.01')
UNKNOWN_NUM = Decimal('NaN')

TX = 'Tx'
INCOME = 'Income'
MINING = 'Mining'
FORK = 'Fork'
BUY = 'Buy'
EXCHANGE_BUY = 'ExBuy'
SELL = 'Sell'
EXCHANGE_SELL = 'ExSell'
GIFT_IN = 'Gift_In'
GIFT_OUT = 'Gift_Out'

LONG = 'Long'
SHORT = 'Short'


def decimal_or_nan(x):
    try:
        return Decimal(x).normalize()
    except InvalidOperation:
        return UNKNOWN_NUM


def standard_sort(df):
    ascending, descending = True, False
    sorting = [
        ('UTC',    ascending),  # i.e. list oldest transactions at the top
        ('amount', ascending),  # i.e. list outputs before inputs (but large outputs first, small inputs first...)
    ]
    by, ordering = zip(*sorting)
    by, ordering = list(by), list(ordering)
    return df.sort_values(by, ascending=ascending, na_position='first')
