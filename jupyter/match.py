from collections import namedtuple
from math import isclose

import pandas as pd

from cryptypes import (UNKNOWN_STR, TX, SELL, INCOME)

_APPLY_TO_ROWS = 1


def match_transactions(df):
    pair_idx = 0
    paired_records = []

    # for each unknown outgoing
    #   filter all unknown tx within a +-1 day range, allowing iteratively looser matches
    #   if any matches exactly, set both to Tx

    df = df.reset_index(drop=True)
    df['_timestamp'] = df.apply(lambda r: r['UTC'].timestamp(), _APPLY_TO_ROWS)
    df_cols = tuple(df.columns)

    instrument_col_idx = 1 + df_cols.index('instrument')
    amount_col_idx =     1 + df_cols.index('amount')
    utc_col_idx =        1 + df_cols.index('UTC')

    # check for transactions with similar values that are close in time
    potential_tx_types = (UNKNOWN_STR,)
    for tx_rel_tolerance in (0.001, 0.01):  # these tolerances determined experimentally by actual tx fee diffs
        for hours_before, hours_after in ((0, 1), (0, 2), (0, 4), (0, 8),
                                          (1, 0), (2, 0), (4, 0), (8, 0)):  # look for transactions near in time first
            unmatched_outs = df[
                (df['amount'] < 0) &
                (df['tx_type'].isin(potential_tx_types)) &
                (df['pair_idx'] == '')
            ]
            # print(f'{len(unmatched_outs)} unmatched outs @{tx_rel_tolerance} tolerance')
            for out_row in unmatched_outs.itertuples():
                out_instr = out_row[instrument_col_idx]
                out_amount = out_row[amount_col_idx]
                out_utc = out_row[utc_col_idx]

                unmatched_ins = df[
                    (df['amount'] > 0) &
                    (df['tx_type'].isin(potential_tx_types)) &
                    (df['pair_idx'] == '') &
                    (df['instrument'] == out_instr) &  # this is key difference between detecting Tx and Ex
                    (out_utc.shift(hours=-hours_before).timestamp() <= df['_timestamp']) &
                    (out_utc.shift(hours=+hours_after).timestamp() >= df['_timestamp'])
                ]

                for in_row in unmatched_ins.itertuples():
                    in_amount = in_row[amount_col_idx]

                    if isclose(in_amount, abs(out_amount), rel_tol=tx_rel_tolerance):
                        # set the pair index for both sides
                        df.loc[df.index == out_row[0], 'tx_type'] = TX
                        df.loc[df.index == out_row[0], 'pair_idx'] = str(pair_idx)
                        df.loc[df.index == in_row[0], 'tx_type'] = TX
                        df.loc[df.index == in_row[0], 'pair_idx'] = str(pair_idx)
                        pair_idx += 1

                        # store a combined pair for alternative view
                        paired_records.append(_pair_out_in(TX, out_row, in_row))
                        # print(f'Tx: (-{hours_before} t +{hours_after}) +-{tx_rel_tolerance*100}% {out_instr} {out_amount} >> {in_amount} @{out_utc}')
                        break

    # get rid of the temporary timestamp
    df = df.drop(columns='_timestamp')

    # create a new DataFrame from the paired records
    paired_df = None
    if len(paired_records):
        paired_df = pd.DataFrame(paired_records, columns=paired_records[0]._fields)

    return df, paired_df


def assume_income_and_sells_for_unknown(df):
    assumed_income = df[(df['tx_type'] == UNKNOWN_STR) & (df['amount'] > 0)]
    assumed_sells = df[(df['tx_type'] == UNKNOWN_STR) & (df['amount'] <= 0)]

    df.loc[(df['tx_type'] == UNKNOWN_STR) & (df['amount'] > 0), 'tx_type'] = INCOME
    df.loc[(df['tx_type'] == UNKNOWN_STR) & (df['amount'] <= 0), 'tx_type'] = SELL
    return df, assumed_income, assumed_sells


def _pair_out_in(tx_type, r_out, r_in):
    return _Pair(tx_type=tx_type,
                 instrument_o=r_out.instrument,                 instrument_i=r_in.instrument,
                 wallet_o=r_out.wallet,                         wallet_i=r_in.wallet,
                 amount_o=r_out.amount,                         amount_i=r_in.amount,
                 amount_USD_o=r_out.amount_USD,                 amount_USD_i=r_in.amount_USD,
                 UTC_o=r_out.UTC,                               UTC_i=r_in.UTC,
                 tx_hash_o=r_out.tx_hash,                       tx_hash_i=r_in.tx_hash,
                 source_notes_o=r_out.source_notes,             source_notes_i=r_in.source_notes)


_Pair = namedtuple('PairedRecord',
                   ('tx_type',
                    'instrument_o', 'instrument_i',
                    'wallet_o', 'wallet_i',
                    'amount_o', 'amount_i',
                    'amount_USD_o', 'amount_USD_i',
                    'UTC_o', 'UTC_i',
                    'tx_hash_o', 'tx_hash_i',
                    'source_notes_o', 'source_notes_i'))
