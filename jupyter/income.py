from cryptypes import INCOME, MINING, FORK, GIFT_IN


INCOME_COLUMNS = [
    'wallet',
    'UTC',
    'instrument',
    'amount_USD',
    'source_notes',
]
_APPLY_TO_ROWS = 1

_income_types = [INCOME, MINING, FORK, GIFT_IN]


def extract(transactions, tax_year, income_inclusion_threshold_usd, gift_exclusion_threshold_usd):
    transactions = transactions[transactions['UTC'].apply(lambda u: u.year == tax_year)]
    transactions = transactions[transactions['tx_type'].isin(_income_types)]
    transactions = transactions[transactions['amount_USD'] > income_inclusion_threshold_usd]
    transactions = _filter_gifts(transactions, gift_exclusion_threshold_usd)
    transactions = transactions[INCOME_COLUMNS]
    return transactions


def _filter_gifts(transactions, threshold_usd):
    usd_accumulator = 0

    def _is_gift_under_threshold(row):
        nonlocal usd_accumulator
        if row['tx_type'] != GIFT_IN:
            return False
        usd_accumulator += row['amount_USD']
        return usd_accumulator < threshold_usd

    filtered = transactions[~transactions.apply(_is_gift_under_threshold, axis=_APPLY_TO_ROWS)]
    return filtered
